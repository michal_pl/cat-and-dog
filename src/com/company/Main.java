package com.company;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {

        Window window1 = new Window();
        window1.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        window1.setVisible(true);
        window1.setSize(new Dimension(800,600));
        window1.setMinimumSize(new Dimension(600,400));
    }
}
