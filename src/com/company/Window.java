package com.company;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Random;

public class Window extends JFrame implements ActionListener {

    private JPanel panelUp;

    private JLabel title, opponent1, opponent2;
    private JLabel leftHitPointsJLabel, rightHitPointsJLabel;
    private JLabel roundNumberLabel;
    private JButton fight, exitButton;
    private ButtonGroup powerUpsLeftSide;
    private ButtonGroup powerUpsRighSide;
    private JRadioButton superAttackLeft, superAttackRight;
    private JRadioButton painKillerLeft;
    private JRadioButton painKillerRight;
    private JRadioButton defaultAttackLeft, defaultAttackRight;
    private JTextField gameInfo;

    private JMenuItem exit;

    //    private int startLeftHitPoints = 10;
//    private int startRightHitPoints = 10;
    private int leftHitPoints = 10;
    private int rightHitPoints = 10;

    private int roundStart = 0;
    private int roundNumber;

    private String nameLeft;
    private String nameRight;

    private JPanel panel1;


    public Window() {

        setSize(800, 600);
        setTitle("Simple Game.");
        //setLayout(new GridLayout(4, 1, 15, 2));
        setLayout(null);

//        JOptionPane.showMessageDialog(null, "Simple battle game, betwen 2 objects. \n " +
//                "Hits are random select. Enjoy!");
//        nameLeft = JOptionPane.showInputDialog(null, "First you have to put name for first opponent: ", " Left");
//        nameRight = JOptionPane.showInputDialog(null, "Second opponent name:", "Right");

        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenuItem save = new JMenuItem("Save");
        JMenuItem load = new JMenuItem("Load");
        JSeparator separator = new JSeparator();
        exit = new JMenuItem("Exit");

        fileMenu.add(save);
        fileMenu.add(load);
        fileMenu.add(separator);
        fileMenu.add(exit);

        exit.addActionListener(this);

        JMenu choseGameMenu = new JMenu("Chose game");
        JMenuItem game1 = new JMenuItem("Game 1");
        JMenuItem game2 = new JMenuItem("Game 2");
        choseGameMenu.add(game1);
        choseGameMenu.add(game2);

        setJMenuBar(menuBar);
        menuBar.add(fileMenu);
        menuBar.add(choseGameMenu);


        title = new JLabel("Fight!!!");
        title.setSize(100, 20);
        title.setFont(new Font("Ara", Font.BOLD, 16));
        add(title);
//        panel1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
//        panel1.add(title);
//        add(panel1);
//
//        JPanel panel2 = new JPanel(new FlowLayout());
//        JPanel panel21 = new JPanel();
//        panel21.setLayout(new BoxLayout(panel21, BoxLayout.Y_AXIS));
        //  JPanel panel22 = new JPanel(new BoxLayout(panel2, BoxLayout.Y_AXIS));

        nameLeft = "Lewy";
        nameRight = "Prawy";

        opponent1 = new JLabel(nameLeft);
        opponent1.setBounds(80, 50, 150, 20);
        opponent1.setFont(new Font("Opponent", Font.ITALIC, 18));
        add(opponent1);


        opponent2 = new JLabel(nameRight);
        opponent2.setBounds(380, 50, 150, 20);
        opponent2.setFont(new Font("Opponent", Font.ITALIC, 18));
        add(opponent2);


        roundNumberLabel = new JLabel("Round number.");
        roundNumberLabel.setBounds(200, 80, 150, 30);
        roundNumberLabel.setFont(new Font("Arial", Font.ITALIC, 16));
        roundNumberLabel.setForeground(Color.BLUE);
        add(roundNumberLabel);


        leftHitPointsJLabel = new JLabel("10");
        leftHitPointsJLabel.setBounds(80, 90, 60, 20);
        add(leftHitPointsJLabel);


//        panel21.add(opponent1);
//        panel21.add(leftHitPointsJLabel);
//        panel2.add(panel21);
//        add(panel2);

        rightHitPointsJLabel = new JLabel("10");
        rightHitPointsJLabel.setBounds(380, 90, 60, 20);
        add(rightHitPointsJLabel);


//        JSeparator separator1 = new JSeparator();
//        separator1.setBounds(10, 40, 600, 2);
//        add(separator1);
//
//        JSeparator separator2 = new JSeparator();
//        separator2.setBounds(10, 110, 600, 2);
//        add(separator2);


        JLabel leftAnimation = new JLabel("Left Animation");
        leftAnimation.setBounds(40, 300, 150, 150);
        add(leftAnimation);

        powerUpsLeftSide = new ButtonGroup();
        powerUpsRighSide = new ButtonGroup();

        superAttackLeft = new JRadioButton("Super atack!");
        superAttackLeft.setBounds(40, 120, 150, 20);
        add(superAttackLeft);
        superAttackLeft.setToolTipText("Super atack take 3 points damage!");

        superAttackRight = new JRadioButton("Super atack");
        superAttackRight.setBounds(380, 120, 150, 20);
        add(superAttackRight);
        superAttackRight.setToolTipText("Super atack take 3 points damage!");

        painKillerLeft = new JRadioButton("Pain killer");
        painKillerLeft.setBounds(40, 150, 150, 20);
        add(painKillerLeft);
        painKillerLeft.setToolTipText("Pain killer can recover 5 HP");

        painKillerRight = new JRadioButton("Pain killer");
        painKillerRight.setBounds(380, 150, 150, 20);
        add(painKillerRight);
        painKillerRight.setToolTipText("Pain killer can recover 5 HP");


        defaultAttackLeft = new JRadioButton("Default attack");
        defaultAttackLeft.setBounds(40, 180, 150, 20);
        add(defaultAttackLeft);
        defaultAttackLeft.setToolTipText("Reduces damage by 2 points");
        defaultAttackLeft.setSelected(true);

        defaultAttackRight = new JRadioButton("Default attack");
        defaultAttackRight.setBounds(380, 180, 150, 20);
        add(defaultAttackRight);
        defaultAttackRight.setToolTipText("Reduces damage by 2 points");
        defaultAttackRight.setSelected(true);

        powerUpsLeftSide.add(superAttackLeft);
        powerUpsRighSide.add(superAttackRight);
        powerUpsLeftSide.add(painKillerLeft);
        powerUpsRighSide.add(painKillerRight);
        powerUpsLeftSide.add(defaultAttackLeft);
        powerUpsRighSide.add(defaultAttackRight);

        fight = new JButton("FIGHT!!");
        fight.setBounds(200, 130, 100, 50);
        add(fight);
        fight.addActionListener(this);

//        exitButton = new JButton("Exit");
//        exit.setBounds(20, 360, 100, 20);
//        add(exit);
//        exit.addActionListener(this);

        gameInfo = new JTextField("Here you can read all information about game.");
        gameInfo.setBounds(160, 250, 450, 50);
        add(gameInfo);


    }


    @Override
    public void actionPerformed(ActionEvent e) {


        Object source = e.getSource();

        Random rn = new Random();
        int answer = rn.nextInt(10) + 1;


        if (source == fight) {
            roundNumber = roundStart + 1;
            roundStart = roundNumber;
            roundNumberLabel.setText("Round number " + roundNumber);

        }

        System.out.println("Runda " + roundNumber + " Losowa liczba" + answer);


        if (source == fight && answer > 5) {


            if (superAttackRight.isSelected()) {

                leftHitPointsJLabel.setText(String.valueOf(leftHitPoints = leftHitPoints - 3));
                gameInfo.setText(nameRight + " super attack " + nameLeft + " take 3 points damage!!!");

            } else if (painKillerRight.isSelected()) {

                rightHitPointsJLabel.setText(String.valueOf(rightHitPoints = rightHitPoints + 5));
                gameInfo.setText(nameRight + " use pain killer and heal 5 HP !!!");

            } else {

                leftHitPointsJLabel.setText(String.valueOf(leftHitPoints = leftHitPoints - 1));
                gameInfo.setText(nameRight + " attack " + nameLeft + " and take 1 point damage.");



            }
            if (leftHitPoints <= 5) {

                if (painKillerRight.isSelected()) {
                    superAttackRight.setEnabled(false);
                } else {
                    superAttackRight.setEnabled(false);
                    defaultAttackRight.setSelected(true);
                }
            }
            if (leftHitPoints <= 0) {
                gameInfo.setText(nameRight + " wins!!!");
                JOptionPane.showMessageDialog(this, nameRight + " wins and save " + rightHitPoints + " hit points! \n\n " +
                        " Thanks for trying my game :) \n" +
                        "New feature will be added soon:)");

                dispose();
            }


        } else if (source == fight && answer <= 5) {

            if (superAttackLeft.isSelected()) {

                rightHitPointsJLabel.setText(String.valueOf(rightHitPoints = rightHitPoints - 3));
                gameInfo.setText(nameLeft + " attack " + nameRight + " and take 3 points damage!!!");

            } else if (painKillerLeft.isSelected()) {

                leftHitPointsJLabel.setText(String.valueOf(leftHitPoints = leftHitPoints + 5));
                gameInfo.setText(nameLeft + " use pain killer and heal 5 HP!!!");
            } else {

                rightHitPointsJLabel.setText(String.valueOf(rightHitPoints = rightHitPoints - 1));
                gameInfo.setText(nameLeft + " attack " + nameRight + " and take 1 point damage!");
            }

            if (rightHitPoints <= 5) {

                if (painKillerLeft.isSelected()) {
                    superAttackLeft.setEnabled(false);
                } else {
                    superAttackLeft.setEnabled(false);
                    defaultAttackLeft.setSelected(true);
                }
            }

            if (rightHitPoints <= 0) {
                gameInfo.setText(nameLeft + " wins!!!");
                JOptionPane.showMessageDialog(this, nameLeft + " wins, and he save " + leftHitPoints + " hit points! \n\n" +
                        "  Thanks for trying my game \n" +
                        "New feature will be added soon:)");
                dispose();
            }


        }
        if (rightHitPoints >= 8) {
            superAttackLeft.setEnabled(true);
        }
        if (leftHitPoints >= 8) {
            superAttackRight.setEnabled(true);
        } else if (source == exit) {
            dispose();
        }


    }
}
