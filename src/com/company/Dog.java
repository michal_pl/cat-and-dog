package com.company;

public class Dog extends Animal  implements Atack {

//    private String name;
//    private int hitPoints, atackPower;

    public Dog (String name, int hitPoints, int atackPower) {
        this.name = name;
        this.hitPoints = hitPoints;
        this.atackPower = atackPower;

    }

//      metoda get i set

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public int getHitPoints() {
//        return hitPoints;
//    }
//
//    public void setHitPoints(int hitPoints) {
//        this.hitPoints = hitPoints;
//    }
//
//    public int getAtackPower() {
//        return atackPower;
//    }
//
//    public void setAtackPower(int atackPower) {
//        this.atackPower = atackPower;
//    }

    @Override
    public void atack(String opponentName) {
        System.out.println(name + " atakuje " + opponentName);
    }

}
